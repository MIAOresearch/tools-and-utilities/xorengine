#pragma once

#include "Proof.hpp"

#if defined(USE_PBP_PROOF)
    #include "prooflogging/pbp/pbp_xor_proof.hpp"
    namespace proof {
        namespace xr = proof::pbp::xr;
    }
#endif

#if defined(USE_DRAT_PROOF)
    #include "prooflogging/drat/drat_xor_proof.hpp"
    namespace proof {
        namespace xr = proof::drat::xr;
    }
#endif

#if !defined(USE_ANY_PROOF)
    #include "prooflogging/without/without_xor_proof.hpp"
    namespace proof {
        namespace xr = proof::without::xr;
    }
#endif