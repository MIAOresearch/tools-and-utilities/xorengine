#pragma once

#include "Proof.hpp"

#if defined(USE_PBP_PROOF)
    #include "prooflogging/pbp/pbp_drat_rules.hpp"
    namespace proof {
        namespace drat_rules = proof::pbp::drat_rules;
    }
#endif

#if defined(USE_DRAT_PROOF)
    #include "prooflogging/drat/drat_drat_rules.hpp"
    namespace proof {
        namespace drat_rules = proof::drat::drat_rules;
    }
#endif

#if !defined(USE_ANY_PROOF)
    #include "prooflogging/without/without_drat_rules.hpp"
    namespace proof {
        namespace drat_rules = proof::without::drat_rules;
    }
#endif