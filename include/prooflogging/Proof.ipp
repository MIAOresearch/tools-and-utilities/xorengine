#pragma once

#include "Proof.hpp"

#if defined(USE_PBP_PROOF)
    #include "prooflogging/pbp/pbp_proof.hpp"
#endif

#if defined(USE_DRAT_PROOF)
    #include "prooflogging/drat/drat_proof.hpp"
#endif

#if !defined(USE_ANY_PROOF)
    #include "prooflogging/without/without_proof.hpp"
#endif