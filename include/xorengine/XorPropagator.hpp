#pragma once

#include <memory>

#include "xorengine/Xor.hpp"
#include "xorengine/SolverAdapter.hpp"
#include "prooflogging/ConstraintId.hpp"

namespace xorp {

template<typename Types>
class XorPropagatorImpl;

template<typename Types>
class XorPropagator {
private:
    std::unique_ptr<XorPropagatorImpl<Types>> pimpl;

public:
    XorPropagator(xorp::Solver<Types>& solver, std::vector<Xor<Types>>& xors, proof::Proof<Types>* proof = nullptr);
    void propagate();
    void notifyBacktrack();
    ~XorPropagator();
};
} // end of namespace