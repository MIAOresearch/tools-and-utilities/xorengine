#pragma once

#include "xorengine/SolverAdapter.hpp"
#include "xorengine/Xor.hpp"

#include <memory>

namespace xorp {
template<typename Clause>
struct VarOccurence;

template<typename Types>
class XorDetector {
private:
    using Var = typename Types::Var;
    using Lit = typename Types::Lit;

public:
    using Occurence = VarOccurence<typename Types::Clause>;
    using OccursVec = std::vector<std::vector<Occurence>>;
    std::unique_ptr<OccursVec> occursPtr;
    OccursVec& occurs;
    // size_t maxSize = 10'000'000;
    size_t maxSize = 10;
    size_t minSize = 0;
    proof::Proof<Types>* proof;

    XorDetector(size_t maxVar, proof::Proof<Types>* _proof = nullptr);
    ~XorDetector();

    void addClause(typename Types::Clause& cls);
    std::vector<Xor<Types>> findXors();
};

} // end of namespace