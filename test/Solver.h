#pragma once
#include <functional>
#include <vector>
#include <iostream>
#include <functional>
#include "xorengine/private/Logging.hpp"
#include "SolverTypes.hpp"

#include "xorengine/SolverAdapter.hpp"
#include "xorengine/XorPropagator.hpp"

namespace testImpl {
    class Solver {
    public:
        size_t maxVar;

        Solver() {}

        virtual bool isSet(Var v) {
            return false;
        }

        virtual bool isTrue(Var v) {
            return false;
        }

        virtual Lit trail(size_t pos) {
            throw "Not implemented!";
        }

        virtual size_t trailSize() {
            return 0;
        }

        virtual void conflict(const xorp::Reason<testImpl::Lit>& clause);

        virtual void enqueue(Lit l, const std::function<xorp::Reason<testImpl::Lit>(Lit l)> getReason);

        virtual void enqueue(Lit l, const xorp::Reason<testImpl::Lit>& reason);

        virtual Var getFreshVar() {
            maxVar += 1;
            return Var(maxVar);
        }
    };
}

namespace std {
    template <>
    struct hash<testImpl::Var>
        {
            size_t operator()(const testImpl::Var & t) const
        {
           std::hash<testImpl::LitData> hashFn;
           return hashFn(t.value);
        }
    };
}
