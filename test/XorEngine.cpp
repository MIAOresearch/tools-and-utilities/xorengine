#include "XorEngine.hpp"

#include "prooflogging/xor_proof.hpp"
#include "xorengine/Xor.ipp"
#include "xorengine/XorDetector.ipp"
#include "xorengine/XorPropagator.ipp"
#include "prooflogging/pbp/pbp_xor_proof.ipp"
#include "prooflogging/drat/drat_xor_proof.ipp"

#include <functional>
#include <vector>
#include "test/Solver.h"
#include "xorengine/private/Logging.hpp"

template class xorp::Xor<xorengine::UsedTypes>;
template class xorp::XorDetector<xorengine::UsedTypes>;
template class xorp::XorPropagator<xorengine::UsedTypes>;
template class xorp::Solver<xorengine::UsedTypes>;

namespace xorp {
    namespace literal {
        template<typename Var, typename Lit>
        inline Var toVar(const Lit& lit) {
            return lit.var();
        }

        template<typename Lit>
        inline bool isNegated(const Lit& lit) {
            return lit.isNegated();
        }

        template<typename Lit>
        inline Lit negated(const Lit& lit) {
            return ~lit;
        }

        template<typename Lit>
        inline bool equals(const Lit& a, const Lit& b) {
            return a == b;
        }

        template<typename Lit>
        inline bool lessThan(const Lit& a, const Lit& b) {
            return static_cast<size_t>(a) < static_cast<size_t>(b);
        }

        template<typename Lit>
        inline Lit undef() {
            return Lit(0);
        }
    }

    namespace variable {
        template<typename Var>
        inline size_t toIndex(const Var& var) {
            return static_cast<size_t>(var);
        }

        template<typename Var>
        inline Var fromIndex(size_t index) {
            return Var(index);
        }

        template<typename Lit, typename Var>
        inline Lit toLit(const Var& var) {
            return Lit(var, false);
        }

        template<typename Lit, typename Var>
        inline Lit toLit(const Var& var, bool isNegated) {
            return Lit(var, isNegated);
        }

        template<typename Var>
        inline bool equals(const Var& a, const Var& b) {
            return a == b;
        }

        template<typename Var>
        inline bool lessThan(const Var& a, const Var& b) {
            return a < b;
        }

    }

    namespace clause {
        template<typename Clause>
        inline auto begin(Clause& clause) {
            return clause.lits.begin();
            // xorengine::UsedTypes::Lit* res = nullptr;
            // return res;
        }

        template<typename Clause>
        inline auto end(Clause& clause) {
            return clause.lits.end();
            // xorengine::UsedTypes::Lit* res = nullptr;
            // return res;
        }

        template<typename Clause>
        inline proof::ConstraintId getId(Clause& clause) {
            return clause.id;
        }
    }

    template<typename Types>
    Solver<Types>::Solver(typename Types::Solver& _solver)
        : solver(_solver)
    {}

    template<typename Types>
    bool Solver<Types>::isSet(typename Types::Var v) {
        return solver.isSet(v);
    }

    template<typename Types>
    bool Solver<Types>::isTrue(typename Types::Var v) {
        return solver.isTrue(v);
    }

    template<typename Types>
    typename Types::Lit Solver<Types>::trail(size_t pos) {
        return solver.trail(pos);
    }

    template<typename Types>
    size_t Solver<Types>::trailSize() {
        return solver.trailSize();
    }

    template<typename Types>
    void Solver<Types>::conflict(const Reason<typename Types::Lit>& reason) {
        return solver.conflict(reason);
    }

    template<typename Types>
    void Solver<Types>::enqueue(typename Types::Lit l, const std::function<Reason<typename Types::Lit>(typename Types::Lit l)> getReason) {
        LOG(trace) << "enqueueing" << EOM;
        solver.enqueue(l, getReason);
    }

    template<typename Types>
    typename Types::Var Solver<Types>::getFreshVar() {
        return solver.getFreshVar();
    }

    template<typename Types>
    LitName Solver<Types>::getLitPrintName(typename Types::Lit lit) {
        return LitName(static_cast<int64_t>(lit));
    }
}