#include "myXor.hpp"
#include "gtest/gtest.h"

TEST(BitSet, SetAndRead_1) {
    int n = 10;
    int i = 2;

    BitSet x(n);
    x.set(i);

    for (int j = 0; j < n; j++) {
        if (j == i) {
            EXPECT_TRUE(x[j]);
        } else {
            EXPECT_FALSE(x[j]);
        }
    }
}

TEST(BitSet, SetAndRead_2) {
    int n = 300;
    int i = 151;

    BitSet x(n);
    for (int j = 0; j < n; j++) {
        EXPECT_FALSE(x[j]);
    }

    x.set(i);

    for (int j = 0; j < n; j++) {
        if (j == i) {
            EXPECT_TRUE(x[j]);
        } else {
            EXPECT_FALSE(x[j]);
        }
    }
}

TEST(BitSet, ResetTrue) {
    size_t n = 300;

    BitSet x(n);
    x.resetToTrue();

    for (size_t i = 0; i < n; i++) {
        EXPECT_TRUE(x[i]);
    }
}

TEST(BitSet, ResetTrueReset) {
    size_t n = 300;

    BitSet x(n);
    x.resetToTrue();
    x.reset();

    for (size_t j = 0; j < n; j++) {
        EXPECT_FALSE(x[j]);
    }
}


TEST(BitSet, UnSetAndRead_1) {
    int n = 10;
    int i = 2;

    BitSet x(n);
    x.resetToTrue();
    x.unset(i);

    for (int j = 0; j < n; j++) {
        if (j == i) {
            EXPECT_FALSE(x[j]);
        } else {
            EXPECT_TRUE(x[j]);
        }
    }
}

TEST(BitSet, SetTo_1) {
    int n = 300;
    int i = 299;

    BitSet x(n);
    EXPECT_FALSE(x[i]);
    x.setTo(i, true);
    EXPECT_TRUE(x[i]);
    x.setTo(i, false);
    EXPECT_FALSE(x[i]);
}


TEST(BitSet, FindAny_empty) {
    size_t n = 10;

    BitSet x(n);
    auto it = x.findAny();
    EXPECT_EQ(it, x.end());
}

TEST(BitSet, FindAny_3) {
    size_t n = 10;
    size_t i = 3;

    BitSet x(n);
    x.set(i);

    auto it = x.findAny();
    EXPECT_NE(it, x.end());
    EXPECT_EQ(it.col(), i);
}

TEST(BitSet, FindAny_last) {
    size_t n = 10;
    size_t i = 9;

    BitSet x(n);
    x.set(i);

    auto it = x.findAny();
    EXPECT_NE(it, x.end());
    EXPECT_EQ(it.col(), i);
}

TEST(BitSet, IteratorLast) {
    size_t n = 10;
    BitSet x(n);
    BitSet::Iterator it = BitSet::Iterator(x, n - 1);
    EXPECT_NE(x.end(), it);
    ++it;
    EXPECT_EQ(x.end(), it);
}

TEST(BitSet, FindAny_emptySpecial) {
    size_t n = 10;

    BitSet x(n);
    x.resetToTrue();
    for (size_t j = 0; j < n; j++) {
        x.unset(j);
    }

    auto it = x.findAny();
    EXPECT_EQ(it, x.end());
}

TEST(BitSet, FindAny_popCountMod2_odd) {
    size_t n = 10;
    std::vector<size_t> set{1, 2, 9};

    BitSet x(n);
    x.resetToTrue();
    for (size_t j = 0; j < n; j++) {
        x.unset(j);
    }

    for (size_t i:set) {
        x.set(i);
    }

    EXPECT_EQ(x.popcountMod2(), set.size() % 2);
}

TEST(BitSet, FindAny_popCountMod2_even) {
    size_t n = 10;
    std::vector<size_t> set{0, 2, 8, 9};

    BitSet x(n);
    x.resetToTrue();
    for (size_t j = 0; j < n; j++) {
        x.unset(j);
    }

    for (size_t i:set) {
        x.set(i);
    }

    EXPECT_EQ(x.popcountMod2(), set.size() % 2);
}

TEST(BitSet, andEq) {
    size_t n = 10;
    std::vector<size_t> set1{0, 2, 8, 9};
    std::vector<size_t> set2{1, 2, 7, 8};
    std::vector<size_t> set3{2, 8};

    BitSet x(n);
    for (size_t i:set1) {
        x.set(i);
    }

    BitSet y(n);
    for (size_t i:set2) {
        y.set(i);
    }

    x &= y;

    size_t j = 0;
    for (size_t i = 0; i < n; i++) {
        if (j < set3.size() && i == set3[j]) {
            EXPECT_TRUE(x[i]);
            j++;
        } else {
            EXPECT_FALSE(x[i]);
        }
    }
}

TEST(BitSet, xorEq) {
    size_t n = 10;
    std::vector<size_t> set1{0, 2, 8, 9};
    std::vector<size_t> set2{1, 2, 7, 8};
    std::vector<size_t> set3{0, 1, 7, 9};

    BitSet x(n);
    for (size_t i:set1) {
        x.set(i);
    }

    BitSet y(n);
    for (size_t i:set2) {
        y.set(i);
    }

    x ^= y;

    size_t j = 0;
    for (size_t i = 0; i < n; i++) {
        if (j < set3.size() && i == set3[j]) {
            EXPECT_TRUE(x[i]);
            j++;
        } else {
            EXPECT_FALSE(x[i]);
        }
    }
}

TEST(BitSet, mixed) {
    size_t n = 10;
    std::vector<size_t> set1{1, 3, 4, 5, 6, 7};
    std::vector<size_t> set2{1, 2, 7, 8};
    std::vector<size_t> set3{0, 1, 7, 9};

    BitSet x(n);
    x.resetToTrue();
    for (size_t i:set1) {
        x.unset(i);
    }

    bool xVal = x.popcountMod2();

    BitSet y(n);
    for (size_t i:set2) {
        y.set(i);
    }

    bool yVal = y.popcountMod2();

    x ^= y;

    EXPECT_EQ(x.popcountMod2(), xVal ^ yVal);

    size_t j = 0;
    for (size_t i = 0; i < n; i++) {
        if (j < set3.size() && i == set3[j]) {
            EXPECT_TRUE(x[i]);
            j++;
        } else {
            EXPECT_FALSE(x[i]);
        }
    }
}