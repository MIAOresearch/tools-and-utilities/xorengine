#pragma once

#include "Solver.h"
#include "SolverTypes.hpp"

#include "xorengine/XorDetector.hpp"
#include "xorengine/XorPropagator.hpp"

class NoLit {};
class NoVar {};
class NoClause {};
class NoSolver {};

    std::ostream& operator<<(std::ostream& os, const NoLit& v);
    std::ostream& operator<<(std::ostream& os, const NoVar& v);

namespace xorengine {
    struct UsedTypes {
        using Lit = testImpl::Lit;
        using Var = testImpl::Var;
        using Clause = testImpl::Clause;
        using Solver = testImpl::Solver;
        // using Lit = NoLit;
        // using Var = NoVar;
        // using Clause = NoClause;
        // using Solver = NoSolver;
        // using Proof = proof::Proof;
    };

    using Detector = xorp::XorDetector<xorengine::UsedTypes>;
    using Propagator = xorp::XorPropagator<xorengine::UsedTypes>;
}

